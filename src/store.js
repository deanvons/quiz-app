import { createStore } from 'vuex'

const store = createStore({
    state: {
        score: 0,
        report: [],
    },
    mutations: {
        setScore: (state, payload) => {
            state.score = payload
        },
        incrementScore: (state, payload) => {
            state.score += payload
        },
        addAnswerToReport: (state, payload) => {
            state.report.push(payload)
        },
    },
})

export default store
