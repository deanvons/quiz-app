import Start from './components/Views/Start.vue'
import Questions from './components/Views/Questions.vue'
import Result from './components/Views/Results.vue'
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        component: Start,
    },
    {
        path: '/questions',
        component: Questions,
    },
    {
        path: '/results',
        component: Result,
    },
]

export default createRouter({
    history: createWebHistory(),
    routes,
})
